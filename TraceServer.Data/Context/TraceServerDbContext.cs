﻿using TraceServer.Domain.Models;
using Microsoft.EntityFrameworkCore;


namespace TraceServer.Data.Context
{
    public class TraceServerDbContext : DbContext
    {
        public TraceServerDbContext(DbContextOptions<TraceServerDbContext> options) : base(options)
        {
        }

        public TraceServerDbContext() : base()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=DESKTOP-49V2LHO;Data Source=localhost;Initial Catalog=TraceServer;User ID=sa;Password=123;");
            }
        }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Rapport> Rapports { get; set; }
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Développeur> Developpeurs { get; set; }
        public DbSet<Application> Applications { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rapport>()
                .HasMany(r => r.Transactions) // Un rapport a plusieurs transactions
                .WithOne(t => t.Rapport) // Une transaction a un rapport
                .HasForeignKey(t => t.RapportId) // Clé étrangère dans Transaction
                .OnDelete(DeleteBehavior.SetNull);




            modelBuilder.Entity<Transaction>()
                .HasOne(t => t.Notification) // Une transaction a une notification
                .WithMany(n => n.Transactions) // Une notification peut avoir plusieurs transactions
                .HasForeignKey(t => t.NotificationId) // Clé étrangère dans Transaction
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
