﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TraceServer.Data.Migrations
{
    public partial class Migration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateCreation",
                table: "Rapports",
                newName: "DateFin");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDeb",
                table: "Rapports",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "details",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDeb",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "details",
                table: "Rapports");

            migrationBuilder.RenameColumn(
                name: "DateFin",
                table: "Rapports",
                newName: "DateCreation");
        }
    }
}
