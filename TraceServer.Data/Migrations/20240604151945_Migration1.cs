﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TraceServer.Data.Migrations
{
    public partial class Migration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Details",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "FileName",
                table: "Rapports");

            migrationBuilder.DropColumn(
                name: "FilePath",
                table: "Rapports");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Details",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FileName",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilePath",
                table: "Rapports",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
