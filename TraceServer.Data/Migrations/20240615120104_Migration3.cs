﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TraceServer.Data.Migrations
{
    public partial class Migration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "details",
                table: "Rapports",
                newName: "Details");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreation",
                table: "Rapports",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreation",
                table: "Rapports");

            migrationBuilder.RenameColumn(
                name: "Details",
                table: "Rapports",
                newName: "details");
        }
    }
}
