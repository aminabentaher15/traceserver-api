﻿using AutoMapper;
using TraceServer.Domain.Models;
using TraceServer.Domain.Dto;
using System.Linq;
using Poulina.TraceServer.Api.Services;

namespace Chaufferie.Api.Mapper
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Notification, NotificationDto>()
                .ForMember(dest => dest.NomApp, opt => opt.MapFrom(src =>
                    src.Transactions.FirstOrDefault() != null ? src.Transactions.FirstOrDefault().Application.NomApp : null))
                .ReverseMap();
            CreateMap<Transaction, TransactionDto>()
            .ForMember(dest => dest.AppFiliale, opt => opt.MapFrom(src =>
                src.Application != null ? src.Application.Filiale : null))
            .ForMember(dest => dest.AppName, opt => opt.MapFrom(src =>
                src.Application != null ? src.Application.NomApp : null))
            .ReverseMap();



        }
    }
}