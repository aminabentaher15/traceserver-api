﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Statistics.Models.Regression.Linear;
using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api
{
    public static class AlgoPrediction
    {
        public static IEnumerable<Prediction> Predict(IEnumerable<Transaction> historicalData)
        {
            var dailyTransactionCounts = CalculateDailyTransactionCounts(historicalData);
            var predictions = new List<Prediction>();

            // Calculer le début de la semaine prochaine après une semaine complète
            var today = DateTime.UtcNow.Date;
            var daysUntilNextMonday = ((int)DayOfWeek.Monday - (int)today.DayOfWeek + 7) % 7;
            var nextWeekStartDate = today.AddDays(daysUntilNextMonday);

            // Utilisation de la régression linéaire multiple pour la prédiction
            var weekdays = new[] { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };

            for (int i = 0; i < 7; i++)
            {
                var currentDay = nextWeekStartDate.AddDays(i);
                var dayOfWeek = currentDay.DayOfWeek;

                // Skip Saturday and Sunday
                if (!weekdays.Contains(dayOfWeek))
                {
                    continue;
                }

                var predictedAdd = PredictDailyTransactionCount(dailyTransactionCounts[dayOfWeek]["Ajout"]);
                var predictedModify = PredictDailyTransactionCount(dailyTransactionCounts[dayOfWeek]["Modification"]);
                var predictedDelete = PredictDailyTransactionCount(dailyTransactionCounts[dayOfWeek]["Suppression"]);

                predictions.Add(new Prediction
                {
                    Date = currentDay,
                    Add = predictedAdd,
                    Modify = predictedModify,
                    Delete = predictedDelete,
                    NbrTransaction = predictedAdd + predictedModify + predictedDelete
                });
            }

            return predictions;
        }

        private static Dictionary<DayOfWeek, Dictionary<string, List<int>>> CalculateDailyTransactionCounts(IEnumerable<Transaction> historicalData)
        {
            var dailyTransactionCounts = new Dictionary<DayOfWeek, Dictionary<string, List<int>>>();

            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                dailyTransactionCounts[day] = new Dictionary<string, List<int>>
                {
                    { "Ajout", new List<int>() },
                    { "Modification", new List<int>() },
                    { "Suppression", new List<int>() }
                };

                var transactionsByDay = historicalData
                    .Where(t => t.Horodatage.DayOfWeek == day)
                    .GroupBy(t => t.Horodatage.Date);

                foreach (var dateGroup in transactionsByDay)
                {
                    dailyTransactionCounts[day]["Ajout"].Add(dateGroup.Count(t => t.Type == "Ajout"));
                    dailyTransactionCounts[day]["Modification"].Add(dateGroup.Count(t => t.Type == "Modification"));
                    dailyTransactionCounts[day]["Suppression"].Add(dateGroup.Count(t => t.Type == "Suppression"));
                }
            }

            return dailyTransactionCounts;
        }

        private static int PredictDailyTransactionCount(List<int> counts)
        {
            if (counts.Count == 0)
                return 0;

            var x = Enumerable.Range(1, counts.Count).Select(i => (double)i).ToArray();
            var y = counts.Select(count => (double)count).ToArray();

            var regression = new MultipleLinearRegression();
            regression.Regress(x.Select(v => new[] { v }).ToArray(), y);

            var nextIndex = counts.Count + 1;
            var predictedCount = regression.Transform(new double[][] { new double[] { nextIndex } })[0];
            return Math.Max(0, (int)Math.Round(predictedCount));
        }
    }
}