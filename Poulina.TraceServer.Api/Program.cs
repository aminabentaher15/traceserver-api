using AutoMapper;
using MathNet.Numerics;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Poulina.TraceServer.Api.Services;
using System;
using System.Collections.Generic;
using System.Text;
using TraceServer.Data.Context;
using TraceServer.Data.Repository;
using TraceServer.Domain.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Trace Server", Version = "v1" });
});

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .SetIsOriginAllowed(origin => true)
            .AllowCredentials());
});

builder.Services.AddDbContext<TraceServerDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Connection")));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = false;
        options.SaveToken = true;
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("veryverysceret.....")),
            ValidateAudience = false,
            ValidateIssuer = false,
            ClockSkew = TimeSpan.Zero
        };
    });

builder.Services.AddAutoMapper(typeof(Program));

builder.Services.AddSignalR();

builder.Services.AddControllers().AddNewtonsoftJson(options =>
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

builder.Services.AddScoped<ITransactionService, TransactionService>();
builder.Services.AddScoped<IPredictionService, PredictionService>();
builder.Services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
builder.Services.AddSingleton<Employ�Service>();
builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<IEmailService, EmailService>();
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Trace Server V1");
    });
}
else
{
    var path = Environment.GetEnvironmentVariable("service");
    var basePath = $":31633/{path}";
    app.UseExceptionHandler("/Error");
    app.UseSwagger(c =>
    {
        c.RouteTemplate = "swagger/{documentName}/swagger.json";
        c.PreSerializeFilters.Add((swaggerDoc, httpReq) => swaggerDoc.Servers = new List<OpenApiServer>
        {
            new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{basePath}" }
        });
    });

    var endpoint = $"/{path}/swagger/v1/swagger.json";
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint(endpoint, "API V1");
    });
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseCors("CorsPolicy");
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHub<ChatHub>("api/chatHub");
});

// Code pour charger le contenu du fichier JSON au d�marrage de l'application
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var dbContext = services.GetRequiredService<TraceServerDbContext>();
    var applicationService = new ApplicationService(dbContext);
    var userIPService = services.GetRequiredService<Employ�Service>();
    var applicationFilePath = @"D:\stage pfe\projet\Poulina.TraceServer\Poulina.TraceServer.Api\Application.json";
    var userIPFilePath = @"D:\stage pfe\projet\Poulina.TraceServer\Poulina.TraceServer.Api\Employ�.json";

    // Charger les applications � partir du fichier JSON
    applicationService.AddApplicationsFromJson(applicationFilePath);

    // Charger les utilisateurs IP � partir du fichier JSON
    userIPService.LoadEmploy�sFromJson(userIPFilePath);
}

app.Run();
