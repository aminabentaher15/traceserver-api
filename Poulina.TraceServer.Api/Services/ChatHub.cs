﻿using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api.Services
{
    using Microsoft.AspNetCore.SignalR;
    using System;
    using System.Threading.Tasks;

    public class ChatHub : Hub
    {
        public async Task SendMessageWithTransaction(Transaction transaction)
        {
            await Clients.All.SendAsync("ReceiveMessage", transaction);
            // Ajoutez un journal pour vérifier si le message est envoyé avec succès
            Console.WriteLine($"Transaction sent: {transaction}");
        }
    }
}
