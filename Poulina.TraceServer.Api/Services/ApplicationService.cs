﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TraceServer.Data.Context;
using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api.Services
{
    public class ApplicationService
    {
        private readonly TraceServerDbContext _dbContext;

        public ApplicationService(TraceServerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddApplicationsFromJson(string filePath)
        {
            try
            {
                // Vérifier si le fichier existe
                if (!File.Exists(filePath))
                {
                    throw new FileNotFoundException("Le fichier JSON spécifié n'a pas été trouvé.", filePath);
                }

                // Vérifier si la table Applications est déjà peuplée
                if (_dbContext.Applications.Any())
                {
                    Console.WriteLine("La table Applications est déjà peuplée. Le contenu du fichier JSON ne sera pas ajouté.");
                    return; // Sortir de la méthode si la table est déjà peuplée
                }

                // Lire le contenu du fichier JSON
                string json = File.ReadAllText(filePath);

                // Désérialiser le JSON en une liste d'objets ApplicationDto
                List<ApplicationDto> applicationDtos = JsonConvert.DeserializeObject<List<ApplicationDto>>(json);

                // Convertir les DTO en entités Application
                List<Application> applications = applicationDtos.Select(dto => new Application
                {
                    AppId = dto.AppId,
                    NomApp = dto.NomApp,
                    Filiale = dto.Filiale,
                    Logo = Convert.FromBase64String(dto.Logo)
                }).ToList();

                // Ajouter chaque application à la base de données
                _dbContext.Applications.AddRange(applications);

                // Enregistrer les modifications dans la base de données
                _dbContext.SaveChanges();
                Console.WriteLine("Les applications ont été ajoutées avec succès à la base de données.");
            }
            catch (FileNotFoundException fnfEx)
            {
                Console.WriteLine($"Erreur : {fnfEx.Message}");
            }
            catch (JsonException jsonEx)
            {
                Console.WriteLine($"Erreur lors de la désérialisation du fichier JSON : {jsonEx.Message}");
            }
            catch (Exception ex)
            {
                // Gérer les erreurs générales
                Console.WriteLine($"Une erreur s'est produite lors de l'ajout des applications à partir du fichier JSON : {ex.Message}");
                throw;
            }
        }

        private class ApplicationDto
        {
            public Guid AppId { get; set; }
            public string NomApp { get; set; }
            public string Filiale { get; set; }
            public string Logo { get; set; }
        }
    }
}
