﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Linq;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api.Services
{
    public class PredictionService : IPredictionService
    {
        private readonly ITransactionService _transactionService;
        private readonly ILogger<PredictionService> _logger;

        public PredictionService(ITransactionService transactionService, ILogger<PredictionService> logger)
        {
            _transactionService = transactionService;
            _logger = logger;
        }

        public async Task<IEnumerable<Prediction>> PredictNextWeek()
        {
            var historicalData = await _transactionService.GetAllTransactions();

            // Utilisez le logger pour vérifier si les transactions sont récupérées
            _logger.LogInformation($"Number of transactions retrieved: {historicalData.Count()}");

            return AlgoPrediction.Predict(historicalData);
        }
    }
}
