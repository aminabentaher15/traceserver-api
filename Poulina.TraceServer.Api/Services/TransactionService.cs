﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TraceServer.Data.Context;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly TraceServerDbContext _dbContext;

        public TransactionService(TraceServerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Transaction>> GetAllTransactions()
        {
            return await _dbContext.Transactions.ToListAsync();
        }


        public async Task<Dictionary<DateTime, Dictionary<string, int>>> GetTransactionCountByTypeDate()
        {
            var transactionCountsByDateAndType = await _dbContext.Transactions
                .GroupBy(t => t.Horodatage.Date)
                .Select(g => new
                {
                    Date = g.Key,
                    CountsByType = g.GroupBy(t => t.Type)
                                    .Select(tg => new { Type = tg.Key, Count = tg.Count() })
                                    .ToDictionary(tg => tg.Type, tg => tg.Count)
                })
                .ToDictionaryAsync(g => g.Date, g => g.CountsByType);

            return transactionCountsByDateAndType;
        }
    }
}
