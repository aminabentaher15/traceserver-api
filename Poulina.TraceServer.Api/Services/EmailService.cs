﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;
using TraceServer.Domain.Interfaces;

namespace Poulina.TraceServer.Api.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<EmailService> _logger;

        public EmailService(IConfiguration configuration, ILogger<EmailService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public async Task SendEmailAsync(string toEmail, string subject, string body)
        {
            var apiKey = _configuration["EmailSettings:SendGridApiKey"];
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress(_configuration["EmailSettings:FromEmail"], _configuration["EmailSettings:FromName"]);
            var to = new EmailAddress(toEmail);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, body, body);
            var response = await client.SendEmailAsync(msg);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                var responseBody = await response.Body.ReadAsStringAsync();
                var errorMessage = $"Failed to send email to {toEmail}. Status code: {response.StatusCode}, Body: {responseBody}";
                _logger.LogError(errorMessage);
                throw new Exception(errorMessage);
            }
        }
    }
}
