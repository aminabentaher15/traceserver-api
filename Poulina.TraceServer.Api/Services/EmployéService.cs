﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TraceServer.Domain.Models;

namespace Poulina.TraceServer.Api.Services
{
    public class EmployéService
    {
        private readonly List<Employé> _employés;

        public EmployéService()
        {
            _employés = new List<Employé>();
        }

        public void LoadEmployésFromJson(string filePath)
        {
            // Charger les données à partir du fichier Employé.json
            string json = File.ReadAllText(filePath);
            _employés.AddRange(JsonConvert.DeserializeObject<List<Employé>>(json));
        }

        public List<Employé> GetAllEmployés()
        {
            // Retourner la liste complète des employés
            return _employés;
        }

        public Employé GetEmployéByIP(string ipAddress)
        {
            // Rechercher l'employé correspondant à l'adresse IP donnée
            return _employés.FirstOrDefault(e => e.AdresseIP == ipAddress);
        }

        public string GetFullNameByIP(string ipAddress)
        {
            // Obtenir le nom complet (nom et prénom concaténés) de l'employé correspondant à l'adresse IP donnée
            var employé = GetEmployéByIP(ipAddress);
            if (employé != null)
            {
                return $"{employé.Nom} {employé.Prenom}";
            }
            else
            {
                return "Employé inconnu";
            }
        }
    }
}
