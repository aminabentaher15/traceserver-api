﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TraceServer.Domain.Interfaces;

namespace Poulina.TraceServer.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PredictionController : ControllerBase
    {
        private readonly IPredictionService _predictionService;

        public PredictionController(IPredictionService predictionService)
        {
            _predictionService = predictionService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPredictionsForNextWeek()
        {
            try
            {
                var predictions = await _predictionService.PredictNextWeek();
                return Ok(predictions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la prédiction pour la semaine prochaine : {ex.Message}");
            }
        }
    }
}
