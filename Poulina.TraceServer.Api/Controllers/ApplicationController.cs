﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using TraceServer.Domain.Handlers;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;
using TraceServer.Domain.Queries;
using System;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Core.Types;
using TraceServer.Domain.Commands;

namespace Poulina.TraceServer.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IGenericRepository<Application> repository;
        private readonly IMapper mapper;

        public ApplicationController(IGenericRepository<Application> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<Application>> getList() =>
            await (new GetListGenericHandler<Application>(repository))
                .Handle(new GetListGenericQuery<Application>(condition: null, includes: x => x.Include(x => x.Transactions)), new CancellationToken());

        [HttpGet("{id}")]
        public async Task<ActionResult<Application>> GetById(Guid id)
        {
            var application = await repository.GetByIdAsync(id);
            if (application == null)
            {
                return NotFound();
            }
            return application;
        }

        [HttpPost("AddApplication")]
        public async Task<Application> Add([FromBody] Application app)
        {
            if (!string.IsNullOrEmpty(app.Base64Logo))
            {
                app.Logo = Convert.FromBase64String(app.Base64Logo);
            }

            var handler = new AddGenericHandler<Application>(repository);
            var command = new AddGenericCommand<Application>(app);

            return await handler.Handle(command, new CancellationToken());
        }

    }
}