﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TraceServer.Domain.Handlers;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;
using TraceServer.Domain.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using System;
using TraceServer.Domain.Commands;
using NuGet.Protocol.Core.Types;
using System.Linq;
using TraceServer.Domain.Dto;
using Polly;
using TraceServer.Data.Context;

namespace Poulina.TraceServer.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly IGenericRepository<Notification> repository;
        private readonly IMapper mapper;
        private readonly TraceServerDbContext _dbContext;

        public NotificationController(IGenericRepository<Notification> repository, IMapper mapper, TraceServerDbContext dbContext)
        {
            this.repository = repository;
            this.mapper = mapper;
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IEnumerable<Notification>> GetList() =>
           await (new GetListGenericHandler<Notification>(repository))
               .Handle(new GetListGenericQuery<Notification>(condition: null, includes: x => x.Include(x => x.Transactions)), new CancellationToken());





        [HttpPost("AddNotification")]
        public async Task<Notification> post([FromBody] Notification notification)
        {
            var handler = new AddGenericHandler<Notification>(repository);
            var command = new AddGenericCommand<Notification>(notification);
            return await handler.Handle(command, new CancellationToken());
        }

        [HttpPut("UpdateNotification")]
        public async Task<Notification> UpdateNotification([FromBody] Notification notification)
        {
            var handler = new PutGenericHandler<Notification>(repository);
            var command = new PutGenericCommand<Notification>(notification);
            return await handler.Handle(command, new CancellationToken());
        }

        [HttpDelete("DeleteNotification/{id}")]
        public async Task<Notification> delete(Guid id)
        {
            var handler = new RemoveGenericHandler<Notification>(repository);
            var command = new RemoveGenericCommand<Notification>(id);
            return await handler.Handle(command, new CancellationToken());
        }

        [HttpGet("{id}")]
        public async Task<Notification> getById(Guid id) =>
            await (new GetGenericHandler<Notification>(repository))
                .Handle(new GetGenericQuery<Notification>(condition: x => x.NotifId == id, includes: x => x.Include(x => x.Transactions)), new CancellationToken());


        [HttpGet("getNotificationDto")]
        public IEnumerable<NotificationDto> GetNotificationDto()
        {
            var notifications = _dbContext.Notifications
                .Include(c => c.Transactions)
                .ThenInclude(cp => cp.Application)
                .ToList();

            return mapper.Map<IEnumerable<NotificationDto>>(notifications);
        }
        [HttpGet("getNotificationByAppName/{appName}")]
        public IEnumerable<NotificationDto> GetNotificationByAppName(string appName)
        {
            var notifications = _dbContext.Notifications
                .Include(c => c.Transactions)
                .ThenInclude(n => n.Application) 
                .Where(n => n.Transactions.FirstOrDefault().Application.NomApp == appName) 
                .ToList();

            return mapper.Map<IEnumerable<NotificationDto>>(notifications);
        }

        [HttpGet("getNotificationById/{notifId}")]
        public async Task<ActionResult<NotificationDto>> GetNotificationById(Guid notifId)
        {
            var notification = await _dbContext.Notifications
                .Include(n => n.Transactions)
                .ThenInclude(t => t.Application)
                .FirstOrDefaultAsync(n => n.NotifId == notifId);

            if (notification == null)
            {
                return NotFound();
            }

            var notificationDto = mapper.Map<NotificationDto>(notification);
            return Ok(notificationDto);
        }
        [HttpGet("totalUnreadCount")]
        public async Task<ActionResult<int>> GetTotalUnreadCount()
        {
            try
            {
                var totalUnreadCount = await _dbContext.Notifications
                    .CountAsync(n => !n.Read);

                return Ok(totalUnreadCount);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération du nombre total de notifications non lues : {ex.Message}");
            }
        }
    
       


}
}
