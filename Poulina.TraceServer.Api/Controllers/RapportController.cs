﻿using Microsoft.AspNetCore.Mvc;
using TraceServer.Domain.Handlers;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;
using TraceServer.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TraceServer.Domain.Commands;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Diagnostics;

namespace Poulina.TraceServer.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RapportController : ControllerBase
    {
        private readonly IGenericRepository<Rapport> _repository;
        private readonly IWebHostEnvironment _env;

        public RapportController(IGenericRepository<Rapport> repository, IWebHostEnvironment env)
        {
            _repository = repository;
            _env = env;
        }

        [HttpGet]
        public async Task<IEnumerable<Rapport>> GetList() =>
            await (new GetListGenericHandler<Rapport>(_repository))
                .Handle(new GetListGenericQuery<Rapport>(condition: null, includes: x => x.Include(x => x.Transactions)), new CancellationToken());

        [HttpPost("AddRapport")]
        public async Task<Rapport> Add([FromBody] Rapport rapport)
        {
            var handler = new AddGenericHandler<Rapport>(_repository);
            var command = new AddGenericCommand<Rapport>(rapport);

            return await handler.Handle(command, new CancellationToken());
        }

        [HttpPost("uploadPdf")]
        public async Task<IActionResult> UploadPdf([FromBody] PdfUploadModel model)
        {
            if (model?.Pdf == null)
            {
                return BadRequest("Données PDF invalides");
            }

            try
            {
                var pdfBytes = Convert.FromBase64String(model.Pdf);
                var fileName = $"rapport_transactions_{Guid.NewGuid()}.pdf";
                var filePath = Path.Combine(_env.ContentRootPath, "uploads", fileName);

                await System.IO.File.WriteAllBytesAsync(filePath, pdfBytes);

                // Ouvrir le dossier après téléchargement
                OpenFolder(Path.Combine(_env.ContentRootPath, "uploads"));

                return Ok(new { filePath, fileName });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erreur interne du serveur : {ex.Message}");
            }
        }

        [HttpPost("uploadCsv")]
        public async Task<IActionResult> UploadCsv([FromBody] CsvUploadModel model)
        {
            if (model?.Csv == null)
            {
                return BadRequest("Données CSV invalides");
            }

            try
            {
                var csvBytes = Convert.FromBase64String(model.Csv);
                var fileName = $"rapport_transactions_{Guid.NewGuid()}.csv";
                var filePath = Path.Combine(_env.ContentRootPath, "uploads", fileName);

                await System.IO.File.WriteAllBytesAsync(filePath, csvBytes);

                // Ouvrir le dossier après téléchargement
                OpenFolder(Path.Combine(_env.ContentRootPath, "uploads"));

                return Ok(new { filePath, fileName });
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Erreur interne du serveur : {ex.Message}");
            }
        }

        private void OpenFolder(string folderPath)
        {
            var psi = new ProcessStartInfo
            {
                FileName = folderPath,
                UseShellExecute = true,
                Verb = "open"
            };
            Process.Start(psi);
        }
    }

}
