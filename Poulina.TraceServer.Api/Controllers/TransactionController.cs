﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Core.Types;
using Poulina.TraceServer.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using TraceServer.Data.Context;
using TraceServer.Domain.Commands;
using TraceServer.Domain.Dto;
using TraceServer.Domain.Handlers;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;
using TraceServer.Domain.Queries;

namespace TraceServer.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly IGenericRepository<Transaction> _repository;
        private readonly IHubContext<ChatHub> _hubContext;
        private readonly TraceServerDbContext _dbContext;
        private readonly IMapper mapper;
        private readonly EmployéService _userIPService;
        private readonly IEmailService _emailService;
        public TransactionController(IGenericRepository<Transaction> repository, IMapper mapper, IHubContext<ChatHub> hubContext, TraceServerDbContext dbContext, EmployéService userIPService, IEmailService emailService)
        {
            _hubContext = hubContext;
            _repository = repository;
            _dbContext = dbContext;
            this.mapper = mapper;
            _userIPService = userIPService;
            _emailService = emailService;
        }

        [HttpGet]
        public async Task<IEnumerable<TransactionDto>> GetTransactions()
        {
            // Obtenir la liste des transactions avec inclusion de la propriété de navigation Application
            var transactions = await _repository.GetListAsync(include: source => source.Include(t => t.Application));

            // Mapper les transactions vers des TransactionDto
            var transactionDtos = mapper.Map<IEnumerable<TransactionDto>>(transactions);

            // Remplir le champ NomUtilisateur de chaque TransactionDto en utilisant UserIPService
            foreach (var transactionDto in transactionDtos)
            {
                transactionDto.Utilisateur = _userIPService.GetEmployéByIP(transactionDto.AdresseIP);
            }

            return transactionDtos;
        }



        [HttpPost("AddTransaction")]
        public async Task<IActionResult> AddTransaction([FromBody] Transaction transaction)
        {
            try
            {
                var hostEntry = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddress = hostEntry.AddressList.FirstOrDefault(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);

                if (ipAddress == null)
                {
                    return StatusCode(500, "No IP address available on this machine.");
                }

                string ipAddressString = ipAddress.ToString();

                var newTransaction = new Transaction
                {
                    TransactionId = Guid.NewGuid(),
                    Horodatage = DateTime.UtcNow,
                    Type = transaction.Type,
                    ApplicationId = transaction.ApplicationId,
                    Details = transaction.Details,
                    AdresseIP = ipAddressString,
                };

                var handler = new AddGenericHandler<Transaction>(_repository);
                var command = new AddGenericCommand<Transaction>(newTransaction);
                var addedTransaction = await handler.Handle(command, new System.Threading.CancellationToken());

                var transactionWithApp = _repository.Get(
                    t => t.TransactionId == addedTransaction.TransactionId,
                    source => source.Include(t => t.Application)
                );

                if (transaction.Type == "Suppression")
                {
                    var deletionNotification = new Notification
                    {
                        NotifId = Guid.NewGuid(),
                        Message = addedTransaction.Details,
                        Date = DateTime.UtcNow,
                        Read = false,
                        Transactions = new List<Transaction> { addedTransaction }
                    };

                    _dbContext.Notifications.Add(deletionNotification);
                    await _dbContext.SaveChangesAsync();

                    var deletionCount = await _dbContext.Transactions
                        .CountAsync(t => t.Type == "Suppression"
                            && t.Horodatage.Date == DateTime.UtcNow.Date
                            && t.ApplicationId == transaction.ApplicationId);

                    if (deletionCount > 10)
                    {
                        var adminEmails = await _dbContext.Developpeurs
                            .Where(u => u.Role == "Admin")
                            .Select(u => u.Email)
                            .ToListAsync();

                        var subject = "Limite quotidienne de transactions de suppression dépassée";
                        var applicationName = transactionWithApp.Application?.NomApp ?? "l'application inconnue";
                        var body = $"Le nombre de transactions de suppression a dépassé la limite de 10 aujourd'hui pour l'application {applicationName}.";

                        foreach (var email in adminEmails)
                        {
                            await _emailService.SendEmailAsync(email, subject, body);
                        }
                    }
                }

                await _hubContext.Clients.All.SendAsync("SendMessageWithTransaction", new
                {
                    addedTransaction.TransactionId,
                    addedTransaction.Horodatage,
                    addedTransaction.Type,
                    addedTransaction.Details,
                    addedTransaction.AdresseIP,
                    addedTransaction.ApplicationId,
                    ApplicationName = transactionWithApp.Application?.NomApp
                });

                return Ok(addedTransaction);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred while adding transaction: {ex}");
                return StatusCode(500, "An error occurred while adding transaction.");
            }
        }


        [HttpGet("GetTransactionsByApp/{applicationId}")]
        public async Task<IActionResult> GetTransactionsByApplication(Guid applicationId)
        {
            try
            {
                // Récupérer les transactions correspondant à l'applicationId avec les informations sur l'application associée
                var transactions = await _repository.GetListAsync(
                    predicate: x => x.ApplicationId == applicationId,
                    include: source => source.Include(t => t.Application),
                    orderBy: null,
                    cancellationToken: new CancellationToken()
                );

                // Mapper les transactions en DTO
                var transactionDtos = mapper.Map<IEnumerable<TransactionDto>>(transactions);

                // Remplir le champ NomUtilisateur de chaque TransactionDto en utilisant UserIPService
                foreach (var transactionDto in transactionDtos)
                {
                    transactionDto.Utilisateur = _userIPService.GetEmployéByIP(transactionDto.AdresseIP);
                }

                // Retourner les transactions DTO
                return Ok(transactionDtos);
            }
            catch (Exception ex)
            {
                // En cas d'erreur, retourner une réponse avec le code d'erreur 500 et un message d'erreur
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions par application : {ex.Message}");
            }
        }


        [HttpGet("GetTransactionsByDate")]
        public async Task<IActionResult> GetTransactionsByDate([FromQuery] DateTime date)
        {
            try
            {
                // Récupérer les transactions pour la date spécifiée
                var transactions = await _dbContext.Transactions.Include(t => t.Application)
                    .Where(x => x.Horodatage.Date == date.Date)
                    .ToListAsync();

                var transactionDtos = mapper.Map<IEnumerable<TransactionDto>>(transactions);

                // Remplir le champ NomUtilisateur de chaque TransactionDto en utilisant UserIPService
                foreach (var transactionDto in transactionDtos)
                {
                    transactionDto.Utilisateur = _userIPService.GetEmployéByIP(transactionDto.AdresseIP);
                }

                return Ok(transactionDtos);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions pour la date spécifiée : {ex.Message}");
            }
        }

        [HttpGet("GetTransactionsByType/{type}")]
        public async Task<IActionResult> GetTransactionsByType(string type)
        {
            try
            {
                var transactions = await _dbContext.Transactions.Include(t => t.Application)
                    .Where(x => x.Type == type)
                    .ToListAsync();

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions par type : {ex.Message}");
            }
        }


        [HttpGet("FilterTransactions")]
        public async Task<IActionResult> FilterTransactions([FromQuery] DateTime? startDate, [FromQuery] DateTime? endDate, [FromQuery] string appName, [FromQuery] string type)
        {
            try
            {
                // Initialiser une requête IQueryable pour récupérer les transactions
                IQueryable<Transaction> query = _dbContext.Transactions.Include(t => t.Application);

                // Appliquer les filtres dynamiquement

                if (startDate.HasValue && endDate.HasValue)
                {
                    // Si les deux dates sont spécifiées, filtrer les transactions dans cette marge de dates
                    query = query.Where(t => t.Horodatage.Date >= startDate.Value.Date && t.Horodatage.Date <= endDate.Value.Date);
                }
                else if (startDate.HasValue)
                {
                    // Si seule la date de début est spécifiée, filtrer les transactions pour cette date uniquement
                    query = query.Where(t => t.Horodatage.Date == startDate.Value.Date);
                }
                // Si seulement la date de fin est spécifiée, vous pouvez choisir de ne rien filtrer ici, car cela signifierait que les transactions peuvent être après la date de fin.

                if (!string.IsNullOrEmpty(appName))
                {
                    // Filtrer par nom de l'application
                    query = query.Where(t => t.Application.NomApp == appName);
                }

                if (!string.IsNullOrEmpty(type))
                {
                    // Filtrer par type de transaction
                    query = query.Where(t => t.Type == type);
                }

                // Exécuter la requête et récupérer les transactions filtrées
                var transactions = await query.ToListAsync();

                // Mapper les transactions vers des DTOs
                var transactionDtos = mapper.Map<IEnumerable<TransactionDto>>(transactions);

                // Remplir le champ NomUtilisateur de chaque TransactionDto en utilisant UserIPService
                foreach (var transactionDto in transactionDtos)
                {
                    transactionDto.Utilisateur = _userIPService.GetEmployéByIP(transactionDto.AdresseIP);
                }

                // Retourner les transactions DTO
                return Ok(transactionDtos);
            }
            catch (Exception ex)
            {
                // En cas d'erreur, retourner une réponse avec le code d'erreur 500 et un message d'erreur
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions pour la date, le nom d'application et le type spécifiés : {ex.Message}");
            }
        }


        [HttpGet("GetDistinctTransactionTypes")]
        public async Task<IActionResult> GetDistinctTransactionTypes()
        {
            try
            {
                // Sélectionnez les types de transaction distincts de la base de données
                var distinctTypes = await _dbContext.Transactions
                    .Select(t => t.Type)
                    .Distinct()
                    .ToListAsync();

                return Ok(distinctTypes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des types de transaction distincts : {ex.Message}");
            }
        }

        [HttpGet("GetTransactionsByDateRange")]
        public IEnumerable<TransactionDto> GetTransactionsByDateRange([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            var transactions = _dbContext.Transactions.Where(t => t.Horodatage >= startDate && t.Horodatage <= endDate)
                .Include(t => t.Application);
            var transactionDtos = mapper.Map<IEnumerable<TransactionDto>>(transactions);

            // Remplir le champ NomUtilisateur de chaque TransactionDto en utilisant UserIPService
            foreach (var transactionDto in transactionDtos)
            {
                transactionDto.Utilisateur = _userIPService.GetEmployéByIP(transactionDto.AdresseIP);
            }

            return transactionDtos;
        }
        // Ajouter une méthode dans le contrôleur TransactionController pour récupérer les transactions par semaine
        [HttpGet("GetTransactionsByWeek")]
        public async Task<IActionResult> GetTransactionsByWeek()
        {
            try
            {
                // Calculer les dates de début et de fin de la semaine actuelle
                var currentDate = DateTime.UtcNow;
                var startDate = currentDate.AddDays(-(int)currentDate.DayOfWeek);
                var endDate = startDate.AddDays(6);

                // Récupérer les transactions pour la semaine spécifiée
                var transactions = await _dbContext.Transactions
                    .Include(t => t.Application)
                    .Where(t => t.Horodatage >= startDate && t.Horodatage <= endDate)
                    .ToListAsync();

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions par semaine : {ex.Message}");
            }
        }

        // Ajouter une méthode dans le contrôleur TransactionController pour récupérer les transactions par mois
        [HttpGet("GetTransactionsByMonth")]
        public async Task<IActionResult> GetTransactionsByMonth()
        {
            try
            {
                // Calculer les dates de début et de fin du mois actuel
                var currentDate = DateTime.UtcNow;
                var startDate = new DateTime(currentDate.Year, currentDate.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);

                // Récupérer les transactions pour le mois spécifié
                var transactions = await _dbContext.Transactions
                    .Include(t => t.Application)
                    .Where(t => t.Horodatage >= startDate && t.Horodatage <= endDate)
                    .ToListAsync();

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions par mois : {ex.Message}");
            }
        }
        // Ajouter une méthode dans le contrôleur TransactionController pour récupérer les transactions de la semaine actuelle
        [HttpGet("GetTransactionsForCurrentWeek")]
        public async Task<IActionResult> GetTransactionsForCurrentWeek()
        {
            try
            {
                // Calculer les dates de début et de fin de la semaine actuelle
                var currentDate = DateTime.UtcNow;
                var startDate = currentDate.Date.AddDays(-(int)currentDate.DayOfWeek);
                var endDate = startDate.AddDays(7).AddSeconds(-1); // La fin de la semaine est le début de la prochaine semaine moins une seconde

                // Récupérer les transactions pour la semaine spécifiée
                var transactions = await _dbContext.Transactions
                    .Where(t => t.Horodatage >= startDate && t.Horodatage <= endDate)
                    .ToListAsync();

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions de la semaine actuelle : {ex.Message}");
            }
        }

        // Ajouter une méthode dans le contrôleur TransactionController pour récupérer les transactions de la semaine précédente
        [HttpGet("GetTransactionsForPreviousWeek")]
        public async Task<IActionResult> GetTransactionsForPreviousWeek()
        {
            try
            {
                // Calculer les dates de début et de fin de la semaine précédente
                var currentDate = DateTime.UtcNow;
                var startDate = currentDate.Date.AddDays(-(int)currentDate.DayOfWeek - 7);
                var endDate = startDate.AddDays(7).AddSeconds(-1); // La fin de la semaine est le début de la prochaine semaine moins une seconde

                // Récupérer les transactions pour la semaine spécifiée
                var transactions = await _dbContext.Transactions
                    .Where(t => t.Horodatage >= startDate && t.Horodatage <= endDate)
                    .ToListAsync();

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Une erreur s'est produite lors de la récupération des transactions de la semaine précédente : {ex.Message}");
            }
        }

        [HttpDelete("DeleteTransaction/{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var handler = new RemoveGenericHandler<Transaction>(_repository);
            var command = new RemoveGenericCommand<Transaction>(id);
            var deletedTransaction = await handler.Handle(command, new System.Threading.CancellationToken());
            return Ok(deletedTransaction);
        }

        [HttpGet("GetTransactionCountsByMonth")]
        public async Task<IActionResult> GetTransactionCountsByMonth()
        {
            try
            {
                // Get the current year
                var currentYear = DateTime.UtcNow.Year;

                // Get the transactions for the current year, grouped by month
                var transactionCounts = await _dbContext.Transactions
                    .Where(t => t.Horodatage.Year == currentYear)
                    .GroupBy(t => t.Horodatage.Month)
                    .Select(g => new
                    {
                        Month = g.Key,
                        Count = g.Count()
                    })
                    .OrderBy(g => g.Month)
                    .ToListAsync();

                // Create a dictionary to hold the counts for each month
                var monthlyTransactionCounts = new Dictionary<string, int>();

                // Initialize the dictionary with all months of the year
                for (int i = 1; i <= 12; i++)
                {
                    var monthName = new DateTime(currentYear, i, 1).ToString("MMM");
                    monthlyTransactionCounts[monthName] = 0;
                }

                // Fill the dictionary with actual counts from the query
                foreach (var transactionCount in transactionCounts)
                {
                    var monthName = new DateTime(currentYear, transactionCount.Month, 1).ToString("MMM");
                    monthlyTransactionCounts[monthName] = transactionCount.Count;
                }

                return Ok(monthlyTransactionCounts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while retrieving transaction counts by month: {ex.Message}");
            }
        }
        [HttpGet("GetTransactionCountByAppAndMonth")]
        public async Task<IActionResult> GetTransactionCountByAppAndMonth()
        {
            try
            {
                // Get the current year
                var currentYear = DateTime.UtcNow.Year;

                // Query to get transaction counts grouped by application name and month
                var transactionCounts = await _dbContext.Transactions
                    .Where(t => t.Horodatage.Year == currentYear)
                    .GroupBy(t => new { t.Application.NomApp, t.Horodatage.Month })
                    .Select(g => new
                    {
                        AppName = g.Key.NomApp,
                        Month = g.Key.Month,
                        Count = g.Count()
                    })
                    .OrderBy(g => g.AppName)
                    .ThenBy(g => g.Month)
                    .ToListAsync();

                // Create a dictionary to hold the counts for each application and month
                var appMonthlyTransactionCounts = new Dictionary<string, Dictionary<string, int>>();

                // Initialize the dictionary with all months of the year for each application
                foreach (var transactionCount in transactionCounts)
                {
                    var appName = transactionCount.AppName;
                    var monthName = new DateTime(currentYear, transactionCount.Month, 1).ToString("MMM");

                    if (!appMonthlyTransactionCounts.ContainsKey(appName))
                    {
                        appMonthlyTransactionCounts[appName] = new Dictionary<string, int>();
                        for (int i = 1; i <= 12; i++)
                        {
                            var month = new DateTime(currentYear, i, 1).ToString("MMM");
                            appMonthlyTransactionCounts[appName][month] = 0;
                        }
                    }

                    appMonthlyTransactionCounts[appName][monthName] = transactionCount.Count;
                }

                return Ok(appMonthlyTransactionCounts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while retrieving transaction counts by application and month: {ex.Message}");
            }
        }

        [HttpGet("GetTransactionsCountByApp")]
        public async Task<IActionResult> GetTransactionsCountByApp()
        {
            try
            {
                // Récupérer les cinq applications ayant le plus grand nombre de transactions
                var top5Apps = await _dbContext.Transactions
                    .GroupBy(t => t.Application.NomApp)
                    .Select(g => new
                    {
                        AppName = g.Key,
                        TransactionCount = g.Count()
                    })
                    .OrderByDescending(g => g.TransactionCount)
                    .Take(10)
                    .ToDictionaryAsync(g => g.AppName, g => g.TransactionCount);

                return Ok(top5Apps);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while retrieving transaction counts by application: {ex.Message}");
            }
        }
        [HttpGet("GetTransactionsCountByType")]
        public async Task<IActionResult> GetTransactionsCountByType()
        {
            try
            {
                // Récupérer le nombre de transactions pour chaque type
                var transactionCountsByType = await _dbContext.Transactions
                    .GroupBy(t => t.Type)
                    .Select(g => new
                    {
                        TransactionType = g.Key,
                        TransactionCount = g.Count()
                    })
                    .ToDictionaryAsync(g => g.TransactionType, g => g.TransactionCount);

                return Ok(transactionCountsByType);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while retrieving transaction counts by type: {ex.Message}");
            }
        }
        [HttpGet("GetTransactionCountByDay")]
        public async Task<IActionResult> GetTransactionCountByDay()
        {
            try
            {
                // Récupérer le nombre de transactions pour chaque jour
                var transactionCountsByDay = await _dbContext.Transactions
                    .GroupBy(t => t.Horodatage.Date)
                    .Select(g => new
                    {
                        TransactionDate = g.Key,
                        TransactionCount = g.Count()
                    })
                    .ToDictionaryAsync(g => g.TransactionDate, g => g.TransactionCount);

                return Ok(transactionCountsByDay);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred while retrieving transaction counts by day: {ex.Message}");
            }
        }


    }
}
