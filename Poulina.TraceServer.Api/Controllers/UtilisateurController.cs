﻿
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Identity;
using TraceServer.Domain.Dto;
using TraceServer.Data.Context;
using TraceServer.Domain.Models;

using System.Collections.Generic;
using TraceServer.Domain.Commands;
using TraceServer.Domain.Handlers;
using TraceServer.Domain.Queries;
using TraceServer.Domain.Interfaces;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.IO;
using Poulina.TraceServer.Api.Services;

namespace AngularAuthYtAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private readonly TraceServerDbContext _authContext;
        private const int RefreshTokenExpiryDays = 5;
        private readonly string _jwtSecret;
        private readonly IGenericRepository<Développeur> _repository;
        private readonly IEmailService _emailService;
        private readonly EmployéService _employéService;
        public UtilisateurController(TraceServerDbContext context, IConfiguration configuration, IGenericRepository<Développeur> repository, IEmailService emailService, EmployéService employéService)
        {
            _repository = repository;
            _authContext = context;
            _jwtSecret = configuration["Jwt:Secret"];
            _emailService = emailService;
            _employéService = employéService;
        }

        [HttpGet("GetEmployes")]
        public async Task<IActionResult> GetEmployes()
        {
            // Load all employee data from Employé.json using EmployéService
            _employéService.LoadEmployésFromJson("employé.json"); // Replace "employé.json" with the actual file path

            // Get all employees
            var allEmployés = _employéService.GetAllEmployés().ToList();

            // Wrap the OkObjectResult in a Task.FromResult
            return await Task.FromResult(Ok(allEmployés));
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(string email, string motDePasse)
        {
            var user = await _authContext.Developpeurs.FirstOrDefaultAsync(u => u.Email == email);

            if (user == null || !user.IsApproved)
            {
                var message = user == null ? "Développeur non trouvé!" : "Développeur n'est pas approuvé!";
                return BadRequest(new { Message = message });
            }

            if (user.FailedLoginAttempts >= 3 && user.LastFailedLoginAttempt.HasValue &&
                user.LastFailedLoginAttempt.Value.AddMinutes(15) > DateTime.UtcNow)
            {
                await _emailService.SendEmailAsync(email, "Alert: La tentative de connexion a échoué plusieurs fois",
                    "Bonjour,\n\n" +
                    "Votre compte sur TraceServer a été verrouillé en raison de plusieurs tentatives de connexion infructueuses.\n\n" +
                    "Pour des raisons de sécurité, votre compte restera verrouillé pendant les 15 prochaines minutes. Veuillez réessayer de vous connecter après cette période.\n\n" +
                    "Si vous n'êtes pas à l'origine de ces tentatives de connexion infructueuses, veuillez contacter le support technique.\n\n" +
                    "Cordialement,\n" +
                    "L'équipe de TraceServer");

                return BadRequest(new { Message = "Votre compte est verrouillé en raison de plusieurs tentatives de connexion échouées. Veuillez réessayer plus tard." });
            }

            var passwordHasher = new PasswordHasher<Développeur>();
            var passwordVerificationResult = passwordHasher.VerifyHashedPassword(new Développeur { MotDePasse = user.MotDePasse }, user.MotDePasse, motDePasse);

            if (passwordVerificationResult != PasswordVerificationResult.Success)
            {
                user.FailedLoginAttempts++;
                user.LastFailedLoginAttempt = DateTime.UtcNow;
                return BadRequest(new { Message = "Mot de passe incorrect." });
            }

            user.FailedLoginAttempts = 0;
            user.LastFailedLoginAttempt = null;
            var accessToken = CreateJwt(user.Email, user.Role);
            var refreshToken = CreateRefreshToken();

            user.Token = accessToken;
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(RefreshTokenExpiryDays);

            _authContext.Developpeurs.Attach(user);
            _authContext.Entry(user).Property(x => x.Token).IsModified = true;
            _authContext.Entry(user).Property(x => x.RefreshToken).IsModified = true;
            _authContext.Entry(user).Property(x => x.RefreshTokenExpiryTime).IsModified = true;

            try
            {
                await _authContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                // Gérer les erreurs de sauvegarde ici
                return StatusCode(500, new { Message = "Erreur lors de la sauvegarde des modifications de l'Développeur." });
            }

            return Ok(new TokenApiDto()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            });
        }

        private string CreateJwt(string email, string role)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSecret);
            var identity = new ClaimsIdentity(new Claim[]
            {
        new Claim(ClaimTypes.Email, email),
        new Claim(ClaimTypes.Role, role)
            });

            var credentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = credentials
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private string CreateJwt(Développeur user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSecret);
            var identity = new ClaimsIdentity(new Claim[]
            {
        new Claim(ClaimTypes.Email, user.Email),
        new Claim(ClaimTypes.Role, user.Role)
            });

            var credentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.UtcNow.AddHours(3), // Set token expiration time
                SigningCredentials = credentials
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] Développeur user)
        {
            if (user == null || string.IsNullOrEmpty(user.Role))
                return BadRequest();

            var roleName = user.Role;

            if (await CheckEmailExistAsync(user.Email))
                return BadRequest(new { Message = "Email Already Exist" });

            var passMessage = CheckPasswordStrength(user.MotDePasse);
            if (!string.IsNullOrEmpty(passMessage))
                return BadRequest(new { Message = passMessage });

            if (!string.IsNullOrEmpty(user.ImagePath))
            {
                // Lire le contenu de l'image
                byte[] imageBytes = System.IO.File.ReadAllBytes(user.ImagePath);
                // Convertir l'image en une chaîne base64
                user.ImageFileName = Path.GetFileName(user.ImagePath);
                user.ImagePath = Convert.ToBase64String(imageBytes);
            }

            // Vérifier si le rôle est valide
            var validRoles = new List<string> { "admin", "agent de controle" };
            if (!validRoles.Contains(roleName.ToLower()))
                return BadRequest(new { Message = "Rôle invalide" });

            var passwordHasher = new PasswordHasher<Développeur>();
            user.MotDePasse = passwordHasher.HashPassword(user, user.MotDePasse);

            // Générer un nouveau UserId
            user.UserId = Guid.NewGuid();

            // Ajouter l'Développeur à la base de données
            _authContext.Developpeurs.Add(user);

            // Après avoir enregistré l'Développeur dans la base de données
            await _authContext.SaveChangesAsync();

            // Retournez l'ID de l'Développeur dans la réponse
            return Ok(new
            {
                Status = 200,
                Message = "Développeur ajouté!",
                UserId = user.UserId
            });
        }



        private async Task<bool> CheckEmailExistAsync(string email)
            => await _authContext.Developpeurs.AnyAsync(x => x.Email == email);


        private static string CheckPasswordStrength(string pass)
        {
            StringBuilder sb = new StringBuilder();
            if (pass.Length < 9)
                sb.Append("La longueur minimale du mot de passe doit être de 8 caractères.\n");
            if (!(Regex.IsMatch(pass, "[a-z]") && Regex.IsMatch(pass, "[A-Z]") && Regex.IsMatch(pass, "[0-9]")))
                sb.Append("Le mot de passe doit être alphanumérique.\n");
            if (!Regex.IsMatch(pass, "[<,>,@,!,#,$,%,^,&,*,(,),_,+,\\[,\\],{,},?,:,;,|,',\\,.,/,~,`,-,=]"))
                sb.Append("Le mot de passe doit contenir des caractères spéciaux.\n");
            return sb.ToString();
        }




        private string CreateRefreshToken()
        {
            var tokenBytes = new byte[64];
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                rngCryptoServiceProvider.GetBytes(tokenBytes);
                return Convert.ToBase64String(tokenBytes);
            }
        }
        [HttpGet]
        public async Task<ActionResult<Développeur>> GetAllUsers()
        {
            return Ok(await _authContext.Developpeurs.ToListAsync());
        }
        [HttpGet("getUsers")]
        public async Task<ActionResult<IEnumerable<object>>> getUsers()
        {
            var users = await _authContext.Developpeurs
                .Select(u => new
                {
                    u.UserId,
                    u.Nom,
                    u.Prenom,
                    u.Email,
                    u.Role,
                    u.IsApproved,
                    u.MotDePasse,
                    u.Telephone
                })
                .ToListAsync();

            return Ok(users);
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] TokenApiDto tokenApiDto)
        {
            if (tokenApiDto == null)
                return BadRequest("Requête de client invalide");

            var accessToken = tokenApiDto.AccessToken;
            var refreshToken = tokenApiDto.RefreshToken;

            var principal = GetPrincipleFromExpiredToken(accessToken);
            var username = principal.Identity.Name;

            var user = await _authContext.Developpeurs.FirstOrDefaultAsync(u => u.Nom == username);

            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                return BadRequest("Requête invalide");

            var newAccessToken = CreateJwt(user);
            var newRefreshToken = CreateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await _authContext.SaveChangesAsync();

            return Ok(new TokenApiDto()
            {
                AccessToken = newAccessToken,
                RefreshToken = newRefreshToken,
            });
        }

        private ClaimsPrincipal GetPrincipleFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSecret)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Jeton invalide");

            return principal;
        }

        [HttpGet("details")]
        public async Task<ActionResult<Développeur>> GetUserDetailsByEmail([FromQuery] string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return BadRequest("L'email de l'Développeur est requis.");
            }

            var Développeur = await _authContext.Developpeurs
                                                .AsNoTracking()
                                                .Where(u => u.Email == email)
                                                .Select(u => new Développeur
                                                {
                                                    UserId = u.UserId,
                                                    Email = u.Email,
                                                    Nom = u.Nom,
                                                    Prenom = u.Prenom,
                                                    Image = u.Image,
                                                    Role = u.Role,
                                                    Telephone = u.Telephone,
                                                    MotDePasse = u.MotDePasse



                                                })
                                                .FirstOrDefaultAsync();

            if (Développeur == null)
            {
                return NotFound("Développeur non trouvé.");
            }

            return Ok(Développeur);
        }

        [HttpPut("UpdateUtilisateur")]
        public async Task<IActionResult> UpdateDéveloppeur([FromBody] Développeur dev)
        {
            var existingUser = await _authContext.Developpeurs.FindAsync(dev.UserId);
            if (existingUser == null)
            {
                return NotFound();
            }

            bool wasApproved = existingUser.IsApproved;
            existingUser.Nom = dev.Nom;
            existingUser.Prenom = dev.Prenom;
            existingUser.Email = dev.Email;
            existingUser.Telephone = dev.Telephone;
            existingUser.Role = dev.Role;

            existingUser.ImagePath = dev.ImagePath;
            existingUser.ImageFileName = dev.ImageFileName;
            existingUser.Image = dev.Image;
            existingUser.IsApproved = dev.IsApproved;

            _authContext.Developpeurs.Update(existingUser);
            await _authContext.SaveChangesAsync();

            if (!wasApproved && dev.IsApproved)
            {
                // Attempt to send the email
                try
                {
                    await _emailService.SendEmailAsync(dev.Email, "Compte approuvé", "Votre compte est approuvé. Vous pouvez accéder à TraceServer maintenant.");
                    Console.WriteLine("E-mail envoyé avec succès à : " + dev.Email);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erreur lors de l'envoi de l'e-mail : " + ex.Message);
                }
            }
            else
            {
                Console.WriteLine("Aucun e-mail envoyé car le compte n'est pas approuvé ou l'utilisateur était déjà approuvé.");
            }

            return Ok(existingUser);
        }



        [HttpDelete("DeleteUtilisateur/{id}")]
        public async Task<IActionResult> SupprimerUtilisateur(Guid id)
        {
            var handler = new RemoveGenericHandler<Développeur>(_repository);
            var command = new RemoveGenericCommand<Développeur>(id);
            var deletedUtilisateur = await handler.Handle(command, new System.Threading.CancellationToken());
            return Ok(deletedUtilisateur);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var handler = new GetGenericHandler<Développeur>(_repository);
            var query = new GetGenericQuery<Développeur>(condition: x => x.UserId == id);
            var utilisateur = await handler.Handle(query, new System.Threading.CancellationToken());
            if (utilisateur == null)
            {
                return NotFound();
            }
            return Ok(utilisateur);
        }



    }

}
