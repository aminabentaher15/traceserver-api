﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Utilisateur
    {
        [Key]
        public Guid UserId { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Email { get; set; }

        public string MotDePasse { get; set; }

        public string Telephone { get; set; }





    }
}
