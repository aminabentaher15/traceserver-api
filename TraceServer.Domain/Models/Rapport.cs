﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Rapport
    {
        [Key]
        public Guid RapportId { get; set; }

        public string Titre { get; set; }
        public string Type { get; set; }
        public string FilePath { get; set; }
        public string Details { get; set; }
        public string FileName { get; set; }
        public DateTime DateDeb { get; set; }
        public DateTime DateFin { get; set; }
        public DateTime DateCreation { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
