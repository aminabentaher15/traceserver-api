﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class CsvUploadModel
    {
        public string Csv { get; set; }
    }
}
