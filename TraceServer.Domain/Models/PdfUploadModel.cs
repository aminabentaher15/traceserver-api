﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class PdfUploadModel
    {
        public string Pdf { get; set; }
    }
}
