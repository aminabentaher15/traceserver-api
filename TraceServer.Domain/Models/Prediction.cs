﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Prediction
    {

        public DateTime Date { get; set; }
        public int Add { get; set; }
        public int Modify { get; set; }
        public int Delete { get; set; }
        public int NbrTransaction { get; set; }
    }
}


