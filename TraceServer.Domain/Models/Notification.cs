﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Notification
    {
        [Key]
        public Guid NotifId { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }
        public bool Read { get; set; }
        [JsonIgnore]
        public virtual ICollection<Transaction> Transactions { get; set; }

    }
}
