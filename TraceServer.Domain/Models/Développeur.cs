﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Développeur : Utilisateur
    {
        public string Role { get; set; }
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
        public string? Token { get; set; }
        public string? ImagePath { get; set; }

        public string? ImageFileName { get; set; }
        public string Image { get; set; }

        public bool IsApproved { get; set; }
        public int FailedLoginAttempts { get; set; }
        public DateTime? LastFailedLoginAttempt { get; set; }
    }
}
