﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TraceServer.Domain.Models
{
    public class Transaction
    {
        [Key]
        public Guid TransactionId { get; set; }

        public DateTime Horodatage { get; set; }

        public string Type { get; set; }

        public string AdresseIP { get; set; }

        public string Details { get; set; }
        public Guid? NotificationId { get; set; }
        public virtual Notification Notification { get; set; }
        public Guid? RapportId { get; set; }
        public virtual Rapport Rapport { get; set; }
        public Guid ApplicationId { get; set; }
        public virtual Application Application { get; set; }
    }
}
