﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Application
    {
        [Key]
        public Guid AppId { get; set; }

        public string NomApp { get; set; }

        public string Filiale { get; set; }
        [NotMapped]
        public string Base64Logo { get; set; }
        public byte[] Logo { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }

}
