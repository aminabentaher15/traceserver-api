﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraceServer.Domain.Models
{
    public class Employé : Utilisateur
    {
        public string AdresseIP { get; set; }
        public string Filiale { get; set; }
        public string Localisation { get; set; }
}

}
