using System.Threading;
using System.Threading.Tasks;
using TraceServer.Domain.Commands;
using TraceServer.Domain.Interfaces;
using MediatR;

namespace TraceServer.Domain.Handlers
{
    public class PutGenericHandler<TEntity> : IRequestHandler<PutGenericCommand<TEntity>, TEntity> where TEntity : class
    {
        private readonly IGenericRepository<TEntity> Repository;
        public PutGenericHandler(IGenericRepository<TEntity> Repository)
        {
            this.Repository = Repository;
        }
        public async Task<TEntity> Handle(PutGenericCommand<TEntity> request, CancellationToken cancellationToken)
        {
            var result = Repository.Put(request.Entity);
            return await Task.FromResult(result);
        }
    }
}