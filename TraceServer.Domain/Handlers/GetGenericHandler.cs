using System.Threading;
using System.Threading.Tasks;
using TraceServer.Domain.Commands;
using TraceServer.Domain.Interfaces;
using MediatR;
using TraceServer.Domain.Queries;

namespace TraceServer.Domain.Handlers
{
    public class GetGenericHandler<TEntity> : IRequestHandler<GetGenericQuery<TEntity>, TEntity> where TEntity : class
    {
        private readonly IGenericRepository<TEntity> Repository;
        public GetGenericHandler(IGenericRepository<TEntity> Repository)
        {
            this.Repository = Repository;
        }

        public async Task<TEntity> Handle(GetGenericQuery<TEntity> request, CancellationToken cancellationToken)
        {
            var result = Repository.Get(request.Condition, request.Includes);
            return await Task.FromResult(result);
        }
    }
}