using System.Threading;
using System.Threading.Tasks;
using TraceServer.Domain.Commands;
using TraceServer.Domain.Interfaces;
using MediatR;

namespace TraceServer.Domain.Handlers
{
    public class AddGenericHandler<TEntity> : IRequestHandler<AddGenericCommand<TEntity>, TEntity> where TEntity : class
    {
        private readonly IGenericRepository<TEntity> Repository;

        public AddGenericHandler(IGenericRepository<TEntity> Repository)
        {
            this.Repository = Repository;
        }

        public async Task<TEntity> Handle(AddGenericCommand<TEntity> request, CancellationToken cancellationToken)
        {
            var result = Repository.Add(request.Entity);
            return await Task.FromResult(result);
        }
    }
}