﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraceServer.Domain.Models;

namespace TraceServer.Domain.Dto
{
    public class TransactionDto
    {
        public Guid TransactionId { get; set; }

        public DateTime Horodatage { get; set; }

        public string Type { get; set; }

        public string AdresseIP { get; set; }
        public Employé Utilisateur { get; set; }
        public string Details { get; set; }
        public string AppName { get; set; }
        public string AppFiliale { get; set; }

    }
}
