﻿using System;

namespace TraceServer.Domain.Dto
{
    public class NotificationDto
    {
        public Guid NotifId { get; set; }
        public string? Message { get; set; }
        public string NomApp { get; set; }
        public DateTime Date { get; set; }
        public bool Read { get; set; } 
    }
}
