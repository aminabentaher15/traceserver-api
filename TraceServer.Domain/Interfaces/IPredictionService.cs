﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TraceServer.Domain.Models;

namespace TraceServer.Domain.Interfaces
{
    public interface IPredictionService
    {
        Task<IEnumerable<Prediction>> PredictNextWeek();
    }

}
