﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TraceServer.Domain.Models;

namespace TraceServer.Domain.Interfaces
{
    public interface ITransactionService
    {
        Task<IEnumerable<Transaction>> GetAllTransactions();
        Task<Dictionary<DateTime, Dictionary<string, int>>> GetTransactionCountByTypeDate();



    }
}
