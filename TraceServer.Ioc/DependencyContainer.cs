using TraceServer.Data.Repository;
using TraceServer.Domain.Interfaces;
using TraceServer.Domain.Models;
using Microsoft.Extensions.DependencyInjection;

namespace TraceServer.Ioc
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //  services.AddTransient<RequestDbContext>();
            services.AddTransient<IGenericRepository<Application>, GenericRepository<Application>>();
            services.AddTransient<IGenericRepository<Notification>, GenericRepository<Notification>>();
            services.AddTransient<IGenericRepository<Rapport>, GenericRepository<Rapport>>();
            services.AddTransient<IGenericRepository<Transaction>, GenericRepository<Transaction>>();
            services.AddTransient<IGenericRepository<Utilisateur>, GenericRepository<Utilisateur>>();
           
          

        }
    }
}